import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DriverManager {

    private static Logger LOG = Logger.getLogger(DriverManager.class.getName());
    private static DriverManager instance = null;
    private WebDriver driver;

    private DriverManager() {

    }

    public WebDriver openBrowser() {
       LOG.info("Initialize  browser");
        {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        }
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }



   public static DriverManager getInstance() {
        if(instance == null){
           instance = new DriverManager();
       }
       return instance;
   }

}




