import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GmailTest {

    private static Logger LOG = Logger.getLogger(GmailTest.class.getName());
    private WebDriver driver ;
   // public WebDriverWait wait;

    @BeforeMethod
    public void initializeDriver() {
        DriverManager manager = DriverManager.getInstance();
        driver = manager.openBrowser();
        driver.get("https://mail.google.com");
    }

    @Test
    public void createGoogleTest() {
        WebElement loginInput = driver.findElement(By.id("identifierId"));
        LOG.info("Enter email name");
        loginInput.sendKeys("datsiuk0508@gmail.com");
        loginInput.submit();
        LOG.info("Click next button.");
        WebElement nextButton = driver.findElement(By.id("identifierNext"));
        nextButton.click();
        Assert.assertFalse(driver.findElement(By.id("forgotPassword")).isDisplayed());
        LOG.info("Enter password");
        WebElement passwordInput = driver.findElement(By.xpath("//*[contains(@name,'password')]"));
        passwordInput.sendKeys("88wdxterqw");
        passwordInput.submit();
        LOG.info("Click next button.");
        WebElement nextPasswordButton = driver.findElement(By.xpath("//*[@id='passwordNext']"));
        nextPasswordButton.click();



    }

    @AfterMethod
    public void driverTearDown() {
        driver.quit();
    }


}